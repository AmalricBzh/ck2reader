<?php
/**
 * Created by PhpStorm.
 * User: xyeh657
 * Date: 11/01/2019
 * Time: 16:24
 */

include_once 'ArmyId.php';
include_once 'ArmySubUnit.php';

class Army
{

    const OBJ_ME =      'ME' ;
    const OBJ_ID =      'ID' ;
    const OBJ_SUB_UNIT ='SUB_UNIT' ;
    const OBJ_FLANK     = 'FLANK';

    protected $currentObject = self::OBJ_ME ;

    protected $id ;
    protected $name ;
    protected $subUnits ;
    protected $location ;
    protected $previous;
    protected $flank ;
    protected $flank_center ;
    protected $flank_right ;
    protected $flank_left ;
    protected $base ;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Army
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Army
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return ArmySubUnit
     */
    public function getSubUnit()
    {
        return $this->subUnits;
    }

    /**
     * @return ArmySubUnit
     */
    public function getLastSubUnit()
    {
        return $this->subUnits[count($this->subUnits) -1];
    }

    /**
     * @param mixed $subUnit
     * @return Army
     */
    public function addSubUnit($subUnit)
    {
        $this->subUnits[] = $subUnit;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @param mixed $location
     * @return Army
     */
    public function setLocation($location)
    {
        $this->location = $location;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFlank()
    {
        return $this->flank;
    }

    /**
     * @param mixed $flank
     * @return Army
     */
    public function setFlank($flank)
    {
        $this->flank = $flank;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSubUnits()
    {
        return $this->subUnits;
    }

    /**
     * @param mixed $subUnits
     * @return Army
     */
    public function setSubUnits($subUnits)
    {
        $this->subUnits = $subUnits;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPrevious()
    {
        return $this->previous;
    }

    /**
     * @param mixed $previous
     * @return Army
     */
    public function setPrevious($previous)
    {
        $this->previous = $previous;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFlankCenter()
    {
        return $this->flank_center;
    }

    /**
     * @param mixed $flank_center
     * @return Army
     */
    public function setFlankCenter($flank_center)
    {
        $this->flank_center = $flank_center;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFlankRight()
    {
        return $this->flank_right;
    }

    /**
     * @param mixed $flank_right
     * @return Army
     */
    public function setFlankRight($flank_right)
    {
        $this->flank_right = $flank_right;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFlankLeft()
    {
        return $this->flank_left;
    }

    /**
     * @param mixed $flank_left
     * @return Army
     */
    public function setFlankLeft($flank_left)
    {
        $this->flank_left = $flank_left;
        return $this;
    }



    /**
     * @return mixed
     */
    public function getBase()
    {
        return $this->base;
    }

    /**
     * @param mixed $base
     * @return Army
     */
    public function setBase($base)
    {
        $this->base = $base;
        return $this;
    }




    /**
     * Renvoie True si je garde la main pour la ligne suivante,
     * Renvoie False si je suis terminé (j'ai trouvé mon accolade fermante)
     * @param $key
     * @param $value
     * @return bool
     */
    public function manage($key, $value)
    {
        $result = true;
        if ($this->currentObject === self::OBJ_ME) {
            switch ($key) {
                case 'id':
                    $this->setId(new ArmyId());
                    $this->currentObject = self::OBJ_ID;
                    break;
                case 'name':
                    $this->setName($value);
                    break;
                case 'sub_unit':
                    $this->addSubUnit(new ArmySubUnit());
                    $this->currentObject = self::OBJ_SUB_UNIT;
                    break;
                case 'previous':
                    $this->setPrevious($value);
                    break;
                case 'location':
                    $this->setLocation($value);
                    break;
                case 'flank':
                    $this->currentObject = self::OBJ_FLANK;
                    break;
                case 'flank_center':
                    $this->setFlankCenter($value);
                    break;
                case 'flank_right':
                    $this->setFlankRight($value);
                    break;
                case 'flank_left':
                    $this->setFlankLeft($value);
                    break;
                case 'base':
                    $this->setBase($value);
                    break;

                case '{':
                    break;
                case '}':
                    $result = false;
                    break;
                default:
                    echo 'ARMY unknown property !!! => ';
                    echo $key . ' => ' . $value . '<br />';
            }
            return $result;
        } else {
            $continue = true ;
            switch ($this->currentObject) {
                case self::OBJ_ID:
                    $continue = $this->getId()->manage($key, $value);
                    break;
                case self::OBJ_SUB_UNIT:
                    $continue = $this->getLastSubUnit()->manage($key, $value);
                    break;

                default:
                    $returnToMe = false ;

                    $values = explode(' ', $key);

                    if (trim($values[count($values)-1]) === '}') {
                        $returnToMe = true ;
                        unset($values[count($values)-1]);
                    }
                    switch ($this->currentObject) {
                        case self::OBJ_FLANK ;
                            $this->setFlank($values) ;
                            break;
                        default:
                            echo 'ARMY Sub-object ('.$this->currentObject.') unknown property !!! => ';
                            echo $key . ' => ' . $value . '<br />';
                    }
                    if ($returnToMe) {
                        $this->currentObject = self::OBJ_ME;
                    }

            }
            // Si je ne continue pas avec le player, je reviens sur moi
            if (!$continue) {
                $this->currentObject = self::OBJ_ME ;
            }
        }

        return $result ;
    }
}
