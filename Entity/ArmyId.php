<?php
/**
 * Created by PhpStorm.
 * User: xyeh657
 * Date: 11/01/2019
 * Time: 16:33
 */

class ArmyId
{

    protected $id ;
    protected $type ;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return ArmyId
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     * @return ArmyId
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * Renvoie True si je garde la main pour la ligne suivante,
     * Renvoie False si je suis terminé (j'ai trouvé mon accolade fermante)
     * @param $key
     * @param $value
     * @return bool
     */
    public function manage($key, $value) {
        $result = true ;
        switch ($key) {
            case 'id':
                $this->setId($value);
                break;
            case 'type':
                $this->setType($value);
                break;
            case '{':
                break;
            case '}':
                $result = false ;
                break;
            default:
                echo 'ARMY ID unknown property !!! => ' ;
                echo $key . ' => ' . $value .'<br />';
        }
        return $result;
    }
}
