<?php
/**
 * Created by PhpStorm.
 * User: xyeh657
 * Date: 11/01/2019
 * Time: 16:36
 */

class ArmySubUnit
{
    const OBJ_ME = 'ME' ;
    const OBJ_ID = 'ID' ;
    const OBJ_TROOPS = 'TROOPS' ;

    protected $currentObject = self::OBJ_ME ;


    protected $id ;
    protected $title ;
    protected $home ;
    protected $type ;
    protected $owner ;
    protected $date ;
    protected $troops ;
    protected $retinue_type ;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return ArmySubUnit
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     * @return ArmySubUnit
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHome()
    {
        return $this->home;
    }

    /**
     * @param mixed $home
     * @return ArmySubUnit
     */
    public function setHome($home)
    {
        $this->home = $home;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     * @return ArmySubUnit
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @param mixed $owner
     * @return ArmySubUnit
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     * @return ArmySubUnit
     */
    public function setDate($date)
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTroops()
    {
        return $this->troops;
    }

    /**
     * @param mixed $troops
     * @return ArmySubUnit
     */
    public function setTroops($troops)
    {
        $this->troops = $troops;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRetinueType()
    {
        return $this->retinue_type;
    }

    /**
     * @param mixed $retinue_type
     * @return ArmySubUnit
     */
    public function setRetinueType($retinue_type)
    {
        $this->retinue_type = $retinue_type;
        return $this;
    }





    /**
     * Renvoie True si je garde la main pour la ligne suivante,
     * Renvoie False si je suis terminé (j'ai trouvé mon accolade fermante)
     * @param $key
     * @param $value
     * @return bool
     */
    public function manage($key, $value)
    {
        $result = true;
        if ($this->currentObject === self::OBJ_ME) {
            switch ($key) {
                case 'id':
                    $this->setId(new ArmyId());
                    $this->currentObject = self::OBJ_ID;
                    break;
                case 'title':
                    $this->setTitle($value);
                    break;
                case 'home':
                    $this->setHome($value);
                    break;
                case 'type':
                    $this->setType($value);
                    break;
                case 'owner':
                    $this->setOwner($value);
                    break;
                case 'date':
                    $this->setDate($value);
                    break;
                case 'retinue_type':
                    $this->setRetinueType($value);
                    break;
                case 'troops':
                    $this->setTroops(new Troops());
                    $this->currentObject = self::OBJ_TROOPS;
                    break;


                case '{':
                    break;
                case '}':
                    $result = false;
                    break;
                default:
                    echo 'ARMY_SUB_UNIT unknown property !!! => ';
                    echo $key . ' => ' . $value . '<br />';
            }
            return $result;
        } else {
            $continue = true ;
            switch ($this->currentObject) {
                case self::OBJ_ID:
                    $continue = $this->getId()->manage($key, $value);
                    break;
                case self::OBJ_TROOPS:
                    $continue = $this->getTroops()->manage($key, $value);
                    break;

                default:
                    echo 'ARMY_SUB_UNIT unknown property !!! => ';
                    echo $key . ' => ' . $value . '<br />';
                    break;
            }
            // Si je ne continue pas avec le player, je reviens sur moi
            if (!$continue) {
                $this->currentObject = self::OBJ_ME ;
            }
        }

        return $result ;
    }

}
