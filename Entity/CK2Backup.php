<?php
/**
 * Created by PhpStorm.
 * User: xyeh657
 * Date: 11/01/2019
 * Time: 10:53
 */

include_once 'Player.php';
include_once 'GameRules.php';
include_once 'DynTitle.php';
include_once 'Flags.php';
include_once 'Dynasties.php';
include_once 'Characters.php';

class CK2Backup
{
    const OBJ_ME            = 'CK2' ;
    const OBJ_PLAYER        = 'PLAYER' ;
    const OBJ_GAME_RULES    = 'GAME_RULES';
    const OBJ_DYN_TITLE     = 'DYN_TITLE';
    const OBJ_FLAGS         = 'FLAGS' ;
    const OBJ_DYNASTIES     = 'DYNASTIES';
    const OBJ_CHARACTERS    = 'CHARACTERS';

    protected $version ;
    protected $date ;
    protected $player ;
    // Realm : Domaine du personnage
    protected $playerRealm;
    protected $baseTitle ;
    protected $isZeusSave ;
    protected $gameRules ;
    protected $gameSpeed ;
    protected $mapMode ;
    protected $dynamicTitles = [] ;
    protected $unit;
    protected $subUnit;
    protected $startDate;
    protected $flags ;
    protected $dynasties;
    protected $characters;

    protected $currentObject = CK2Backup::OBJ_ME ;



    /**
     * @return mixed
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @param mixed $version
     * @return CK2Backup
     */
    public function setVersion($version)
    {
        $this->version = $version;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     * @return CK2Backup
     */
    public function setDate($date)
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return Player
     */
    public function getPlayer()
    {
        return $this->player;
    }

    /**
     * @param Player $player
     * @return CK2Backup
     */
    public function setPlayer($player)
    {
        $this->player = $player;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPlayerRealm()
    {
        return $this->playerRealm;
    }

    /**
     * @param mixed $playerRealm
     * @return CK2Backup
     */
    public function setPlayerRealm($playerRealm)
    {
        $this->playerRealm = $playerRealm;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBaseTitle()
    {
        return $this->baseTitle;
    }

    /**
     * @param mixed $baseTitle
     * @return CK2Backup
     */
    public function setBaseTitle($baseTitle)
    {
        $this->baseTitle = $baseTitle;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsZeusSave()
    {
        return $this->isZeusSave;
    }

    /**
     * @param mixed $isZeusSave
     * @return CK2Backup
     */
    public function setIsZeusSave($isZeusSave)
    {
        $this->isZeusSave = $isZeusSave;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGameRules()
    {
        return $this->gameRules;
    }

    /**
     * @param mixed $gameRules
     * @return CK2Backup
     */
    public function setGameRules($gameRules)
    {
        $this->gameRules = $gameRules;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGameSpeed()
    {
        return $this->gameSpeed;
    }

    /**
     * @param mixed $gameSpeed
     * @return CK2Backup
     */
    public function setGameSpeed($gameSpeed)
    {
        $this->gameSpeed = $gameSpeed;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMapMode()
    {
        return $this->mapMode;
    }

    /**
     * @param mixed $mapMode
     * @return CK2Backup
     */
    public function setMapMode($mapMode)
    {
        $this->mapMode = $mapMode;
        return $this;
    }

    /**
     * @return array
     */
    public function getDynamicTitles()
    {
        return $this->dynamicTitles;
    }
    /**
     * @return DynTitle
     */
    public function getLastDynamicTitle()
    {
        return $this->dynamicTitles[count($this->dynamicTitles)-1];
    }

    /**
     * @param array $dynamicTitle
     * @return CK2Backup
     */
    public function addDynamicTitle($dynamicTitle)
    {
        $this->dynamicTitles[] = $dynamicTitle;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUnit()
    {
        return $this->unit;
    }

    /**
     * @param mixed $unit
     * @return CK2Backup
     */
    public function setUnit($unit)
    {
        $this->unit = $unit;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSubUnit()
    {
        return $this->subUnit;
    }

    /**
     * @param mixed $subUnit
     * @return CK2Backup
     */
    public function setSubUnit($subUnit)
    {
        $this->subUnit = $subUnit;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * @param mixed $startDate
     * @return CK2Backup
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFlags()
    {
        return $this->flags;
    }

    /**
     * @param mixed $flags
     * @return CK2Backup
     */
    public function setFlags($flags)
    {
        $this->flags = $flags;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDynasties()
    {
        return $this->dynasties;
    }

    /**
     * @param mixed $dynasties
     * @return CK2Backup
     */
    public function setDynasties($dynasties)
    {
        $this->dynasties = $dynasties;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCharacters()
    {
        return $this->characters;
    }

    /**
     * @param mixed $characters
     * @return CK2Backup
     */
    public function setCharacters($characters)
    {
        $this->characters = $characters;
        return $this;
    }















    public function manage($key, $value) {
        $result = true;
        if ($this->currentObject === self::OBJ_ME) {
            switch ($key) {
                case 'version':
                    $this->setVersion($value);
                    break;
                case 'date':
                    $this->setDate($value);
                    break;
                case 'player':
                    $this->setPlayer(new Player());
                    $this->currentObject = self::OBJ_PLAYER;
                    break;
                case 'player_realm':
                    $this->setPlayerRealm($value);
                    break;
                case 'base_title':
                    $this->setBaseTitle($value);
                    break;
                case 'is_zeus_save':
                    $this->setIsZeusSave($value);
                    break;
                case 'game_rules':
                    $this->setGameRules(new GameRules());
                    $this->currentObject = self::OBJ_GAME_RULES;
                    break;
                case 'game_speed':
                    $this->setGameSpeed($value);
                    break;
                case 'mapmode':
                    $this->setMapMode($value);
                    break;
                case 'dyn_title':
                    $this->addDynamicTitle(new DynTitle());
                    $this->currentObject = self::OBJ_DYN_TITLE;
                    break;
                case 'unit':
                    $this->setUnit($value);
                    break;
                case 'sub_unit':
                    $this->setSubUnit($value);
                    break;
                case 'start_date':
                    $this->setStartDate($value);
                    break;
                case 'flags':
                    $this->setFlags(new Flags());
                    $this->currentObject = self::OBJ_FLAGS;
                    break;
                case 'dynasties':
                    $this->setDynasties(new Dynasties());
                    $this->currentObject = self::OBJ_DYNASTIES;
                    break;
                case 'character':   // Au singulier !
                    $this->setCharacters(new Characters());
                    $this->currentObject = self::OBJ_CHARACTERS;
                    break;
                default:
                    echo 'CK2 unknown property !!! => ' ;
                    echo $key . ' => ' . $value .'<br />';
                    $result = false ;
            }
        } else {
            $continue = true ;
            switch ($this->currentObject) {
                case self::OBJ_PLAYER :
                    $continue = $this->getPlayer()->manage($key, $value);
                    break;
                case self::OBJ_GAME_RULES :
                    $continue = $this->getGameRules()->manage($key, $value);
                    break;
                case self::OBJ_DYN_TITLE :
                    $continue = $this->getLastDynamicTitle()->manage($key, $value);
                    break ;
                case self::OBJ_FLAGS :
                    $continue = $this->getFlags()->manage($key, $value);
                    break ;
                case self::OBJ_DYNASTIES :
                    $continue = $this->getDynasties()->manage($key, $value);
                    break ;
                case self::OBJ_CHARACTERS :
                    $continue = $this->getCharacters()->manage($key, $value);
                    break ;
                default:
                    echo 'ARGGGGG !!! Unknown current Object : '.$this->currentObject.' !<br />';
                    die;
            }

            // Si je ne continue pas avec le player, je reviens sur moi
            if (!$continue) {
                $this->currentObject = CK2Backup::OBJ_ME ;
            }
        }

        return $result;
    }



}
