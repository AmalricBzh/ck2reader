<?php
/**
 * Created by PhpStorm.
 * User: xyeh657
 * Date: 11/01/2019
 * Time: 13:46
 */

include_once 'CharacterLgr.php';
include_once 'CharacterMd.php';
include_once 'Claim.php';
include_once 'Domain.php';
include_once 'Unborn.php' ;
include_once 'Vars.php';

class Character
{
    const OBJ_ME    = 'ME' ;
    const OBJ_LGR   = 'LGR' ;
    const OBJ_SOCIETY   = 'SOCIETY' ;
    const OBJ_FLAGS = 'FLAGS' ;
    const OBJ_MD    = 'MD' ;
    const OBJ_CLAIM = 'CLAIM' ;
    const OBJ_KNOWN_PLOTS   = 'KNOWN_PLOTS' ;
    const OBJ_DOMAIN        = 'DOMAIN' ;
    const OBJ_UNBORN        = 'UNBORN' ;
    const OBJ_VARS          = 'VARS' ;

    protected $currentObject = self::OBJ_ME;

    // unborn =    => level 0
    // {           => level 1
    //   {         => level 2
    private $level = 0 ;

    protected $id;
    protected $fem;
    protected $bn ;
    protected $bd;
    protected $att ;
    protected $traits ;
    protected $rel ;
    protected $cul ;
    protected $dna ;
    protected $prp ;
    protected $fer;
    protected $health;
    protected $prs;
    protected $piety ;
    protected $job ;
    protected $wealth ;
    protected $emp ;
    protected $host ;
    protected $emi ;
    protected $eme ;
    protected $eyi ;
    protected $eypi ;
    protected $lgr ;
    protected $action ;
    protected $acdate ;
    protected $cpos ;
    protected $cpos_start ;
    protected $society;
    protected $fat ; // Father
    protected $rfat ; // Real father ?
    protected $mot ; // Mother
    protected $dnt ;
    protected $flags ;
    protected $md ;
    protected $bstd ;
    protected $claims ;
    protected $title ;
    protected $ambitionDate ;
    protected $focusDate;
    protected $score ;
    protected $gov ;
    protected $regent ;
    protected $guardian ;
    protected $knownPlots ;
    protected $dmn ;
    protected $acloc ;
    protected $spouse ;
    protected $btrh ;
    protected $g_cul ;
    protected $retrat ;
    protected $nick ;
    protected $lover ;
    protected $unborns ;
    protected $war_target ;
    protected $last_objective ;
    protected $council_discontent_start ;
    protected $council_discontent_update ;
    protected $council_discontent_reason ;
    protected $imprisoned ;
    protected $weak ;
    protected $spawned_bastards ;
    protected $vars ;
    protected $curinc ;
    protected $in_hiding ;


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Character
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBn()
    {
        return $this->bn;
    }

    /**
     * @param mixed $bn
     * @return Character
     */
    public function setBn($bn)
    {
        $this->bn = $bn;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFem()
    {
        return $this->fem;
    }

    /**
     * @param mixed $fem
     * @return Character
     */
    public function setFem($fem)
    {
        $this->fem = $fem;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBd()
    {
        return $this->bd;
    }

    /**
     * @param mixed $bd
     * @return Character
     */
    public function setBd($bd)
    {
        $this->bd = $bd;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAtt()
    {
        return $this->att;
    }

    /**
     * @param mixed $att
     * @return Character
     */
    public function setAtt($att)
    {
        $this->att = $att;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTraits()
    {
        return $this->traits;
    }

    /**
     * @param mixed $traits
     * @return Character
     */
    public function setTraits($traits)
    {
        $this->traits = $traits;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRel()
    {
        return $this->rel;
    }

    /**
     * @param mixed $rel
     * @return Character
     */
    public function setRel($rel)
    {
        $this->rel = $rel;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCul()
    {
        return $this->cul;
    }

    /**
     * @param mixed $cul
     * @return Character
     */
    public function setCul($cul)
    {
        $this->cul = $cul;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDna()
    {
        return $this->dna;
    }

    /**
     * @param mixed $dna
     * @return Character
     */
    public function setDna($dna)
    {
        $this->dna = $dna;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPrp()
    {
        return $this->prp;
    }

    /**
     * @param mixed $prp
     * @return Character
     */
    public function setPrp($prp)
    {
        $this->prp = $prp;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFer()
    {
        return $this->fer;
    }

    /**
     * @param mixed $fer
     * @return Character
     */
    public function setFer($fer)
    {
        $this->fer = $fer;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHealth()
    {
        return $this->health;
    }

    /**
     * @param mixed $health
     * @return Character
     */
    public function setHealth($health)
    {
        $this->health = $health;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPrs()
    {
        return $this->prs;
    }

    /**
     * @param mixed $prs
     * @return Character
     */
    public function setPrs($prs)
    {
        $this->prs = $prs;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPiety()
    {
        return $this->piety;
    }

    /**
     * @param mixed $piety
     * @return Character
     */
    public function setPiety($piety)
    {
        $this->piety = $piety;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getJob()
    {
        return $this->job;
    }

    /**
     * @param mixed $job
     * @return Character
     */
    public function setJob($job)
    {
        $this->job = $job;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getWealth()
    {
        return $this->wealth;
    }

    /**
     * @param mixed $wealth
     * @return Character
     */
    public function setWealth($wealth)
    {
        $this->wealth = $wealth;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmp()
    {
        return $this->emp;
    }

    /**
     * @param mixed $emp
     * @return Character
     */
    public function setEmp($emp)
    {
        $this->emp = $emp;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * @param mixed $host
     * @return Character
     */
    public function setHost($host)
    {
        $this->host = $host;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmi()
    {
        return $this->emi;
    }

    /**
     * @param mixed $emi
     * @return Character
     */
    public function setEmi($emi)
    {
        $this->emi = $emi;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEyi()
    {
        return $this->eyi;
    }

    /**
     * @param mixed $eyi
     * @return Character
     */
    public function setEyi($eyi)
    {
        $this->eyi = $eyi;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEypi()
    {
        return $this->eypi;
    }

    /**
     * @param mixed $eypi
     * @return Character
     */
    public function setEypi($eypi)
    {
        $this->eypi = $eypi;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLgr()
    {
        return $this->lgr;
    }

    /**
     * @param mixed $lgr
     * @return Character
     */
    public function setLgr($lgr)
    {
        $this->lgr = $lgr;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @param mixed $action
     * @return Character
     */
    public function setAction($action)
    {
        $this->action = $action;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAcdate()
    {
        return $this->acdate;
    }

    /**
     * @param mixed $acdate
     * @return Character
     */
    public function setAcdate($acdate)
    {
        $this->acdate = $acdate;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCpos()
    {
        return $this->cpos;
    }

    /**
     * @param mixed $cpos
     * @return Character
     */
    public function setCpos($cpos)
    {
        $this->cpos = $cpos;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCposStart()
    {
        return $this->cpos_start;
    }

    /**
     * @param mixed $cpos_start
     * @return Character
     */
    public function setCposStart($cpos_start)
    {
        $this->cpos_start = $cpos_start;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSociety()
    {
        return $this->society;
    }

    /**
     * @param mixed $society
     * @return Character
     */
    public function setSociety($society)
    {
        $this->society = $society;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFat()
    {
        return $this->fat;
    }

    /**
     * @param mixed $fat
     * @return Character
     */
    public function setFat($fat)
    {
        $this->fat = $fat;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMot()
    {
        return $this->mot;
    }

    /**
     * @param mixed $mot
     * @return Character
     */
    public function setMot($mot)
    {
        $this->mot = $mot;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDnt()
    {
        return $this->dnt;
    }

    /**
     * @param mixed $dnt
     * @return Character
     */
    public function setDnt($dnt)
    {
        $this->dnt = $dnt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFlags()
    {
        return $this->flags;
    }

    /**
     * @param mixed $flags
     * @return Character
     */
    public function setFlags($flags)
    {
        $this->flags = $flags;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMd()
    {
        return $this->md;
    }

    public function getLastMd()
    {
        return $this->md[count($this->md)-1];
    }

    /**
     * @param mixed $md
     * @return Character
     */
    public function addMd($md)
    {
        $this->md[] = $md;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRfat()
    {
        return $this->rfat;
    }

    /**
     * @param mixed $rfat
     * @return Character
     */
    public function setRfat($rfat)
    {
        $this->rfat = $rfat;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBstd()
    {
        return $this->bstd;
    }

    /**
     * @param mixed $bstd
     * @return Character
     */
    public function setBstd($bstd)
    {
        $this->bstd = $bstd;
        return $this;
    }

    /**
     * @return Claim[]
     */
    public function getClaims()
    {
        return $this->claims;
    }

    /**
     * @return Claim
     */
    public function getLastClaim()
    {
        return $this->claims[count($this->claims) -1];
    }

    /**
     * @param Claim $claim
     * @return Character
     */
    public function addClaim($claim)
    {
        $this->claims[] = $claim;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEme()
    {
        return $this->eme;
    }

    /**
     * @param mixed $eme
     * @return Character
     */
    public function setEme($eme)
    {
        $this->eme = $eme;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     * @return Character
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAmbitionDate()
    {
        return $this->ambitionDate;
    }

    /**
     * @param mixed $ambitionDate
     * @return Character
     */
    public function setAmbitionDate($ambitionDate)
    {
        $this->ambitionDate = $ambitionDate;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFocusDate()
    {
        return $this->focusDate;
    }

    /**
     * @param mixed $focusDate
     * @return Character
     */
    public function setFocusDate($focusDate)
    {
        $this->focusDate = $focusDate;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getScore()
    {
        return $this->score;
    }

    /**
     * @param mixed $score
     * @return Character
     */
    public function setScore($score)
    {
        $this->score = $score;
        return $this;
    }



    /**
     * @return mixed
     */
    public function getGov()
    {
        return $this->gov;
    }

    /**
     * @param mixed $gov
     * @return Character
     */
    public function setGov($gov)
    {
        $this->gov = $gov;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRegent()
    {
        return $this->regent;
    }

    /**
     * @param mixed $regent
     * @return Character
     */
    public function setRegent($regent)
    {
        $this->regent = $regent;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGuardian()
    {
        return $this->guardian;
    }

    /**
     * @param mixed $guardian
     * @return Character
     */
    public function setGuardian($guardian)
    {
        $this->guardian = $guardian;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getKnownPlots()
    {
        return $this->knownPlots;
    }

    /**
     * @param mixed $knownPlots
     * @return Character
     */
    public function setKnownPlots($knownPlots)
    {
        $this->knownPlots = $knownPlots;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDmn()
    {
        return $this->dmn;
    }

    /**
     * @param mixed $dmn
     */
    public function setDmn($dmn)
    {
        $this->dmn = $dmn;
    }

    /**
     * @return string
     */
    public function getCurrentObject()
    {
        return $this->currentObject;
    }

    /**
     * @param string $currentObject
     * @return Character
     */
    public function setCurrentObject($currentObject)
    {
        $this->currentObject = $currentObject;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAcloc()
    {
        return $this->acloc;
    }

    /**
     * @param mixed $acloc
     * @return Character
     */
    public function setAcloc($acloc)
    {
        $this->acloc = $acloc;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSpouse()
    {
        return $this->spouse;
    }

    /**
     * @param mixed $spouse
     * @return Character
     */
    public function setSpouse($spouse)
    {
        $this->spouse = $spouse;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBtrh()
    {
        return $this->btrh;
    }

    /**
     * @param mixed $btrh
     * @return Character
     */
    public function setBtrh($btrh)
    {
        $this->btrh = $btrh;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGCul()
    {
        return $this->g_cul;
    }

    /**
     * @param mixed $g_cul
     * @return Character
     */
    public function setGCul($g_cul)
    {
        $this->g_cul = $g_cul;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRetrat()
    {
        return $this->retrat;
    }

    /**
     * @param mixed $retrat
     * @return Character
     */
    public function setRetrat($retrat)
    {
        $this->retrat = $retrat;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNick()
    {
        return $this->nick;
    }

    /**
     * @param mixed $nick
     * @return Character
     */
    public function setNick($nick)
    {
        $this->nick = $nick;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLover()
    {
        return $this->lover;
    }

    /**
     * @param mixed $lover
     * @return Character
     */
    public function setLover($lover)
    {
        $this->lover = $lover;
        return $this;
    }

    /**
     * @return Unborn[]
     */
    public function getUnborns()
    {
        return $this->unborns;
    }

    /**
     * @param Unborn $unborns
     * @return Character
     */
    public function addUnborn($unborn)
    {
        $this->unborns[] = $unborn;
        return $this;
    }

    /**
     * @return Unborn
     */
    public function getLastUnborn()
    {
        return $this->unborns[count($this->unborns) - 1];
    }

    /**
     * @return mixed
     */
    public function getWarTarget()
    {
        return $this->war_target;
    }

    /**
     * @param mixed $war_target
     * @return Character
     */
    public function setWarTarget($war_target)
    {
        $this->war_target = $war_target;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLastObjective()
    {
        return $this->last_objective;
    }

    /**
     * @param mixed $last_objective
     * @return Character
     */
    public function setLastObjective($last_objective)
    {
        $this->last_objective = $last_objective;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCouncilDiscontentStart()
    {
        return $this->council_discontent_start;
    }

    /**
     * @param mixed $council_discontent_start
     * @return Character
     */
    public function setCouncilDiscontentStart($council_discontent_start)
    {
        $this->council_discontent_start = $council_discontent_start;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCouncilDiscontentUpdate()
    {
        return $this->council_discontent_update;
    }

    /**
     * @param mixed $council_discontent_update
     * @return Character
     */
    public function setCouncilDiscontentUpdate($council_discontent_update)
    {
        $this->council_discontent_update = $council_discontent_update;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCouncilDiscontentReason()
    {
        return $this->council_discontent_reason;
    }

    /**
     * @param mixed $council_discontent_reason
     * @return Character
     */
    public function setCouncilDiscontentReason($council_discontent_reason)
    {
        $this->council_discontent_reason = $council_discontent_reason;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getImprisoned()
    {
        return $this->imprisoned;
    }

    /**
     * @param mixed $imprisoned
     * @return Character
     */
    public function setImprisoned($imprisoned)
    {
        $this->imprisoned = $imprisoned;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getWeak()
    {
        return $this->weak;
    }

    /**
     * @param mixed $weak
     * @return Character
     */
    public function setWeak($weak)
    {
        $this->weak = $weak;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSpawnedBastards()
    {
        return $this->spawned_bastards;
    }

    /**
     * @param mixed $spawned_bastards
     * @return Character
     */
    public function setSpawnedBastards($spawned_bastards)
    {
        $this->spawned_bastards = $spawned_bastards;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getVars()
    {
        return $this->vars;
    }

    /**
     * @param mixed $vars
     * @return Character
     */
    public function setVars($vars)
    {
        $this->vars = $vars;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCurinc()
    {
        return $this->curinc;
    }

    /**
     * @param mixed $curinc
     * @return Character
     */
    public function setCurinc($curinc)
    {
        $this->curinc = $curinc;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getInHiding()
    {
        return $this->in_hiding;
    }

    /**
     * @param mixed $in_hiding
     * @return Character
     */
    public function setInHiding($in_hiding)
    {
        $this->in_hiding = $in_hiding;
        return $this;
    }








    /**
     * Renvoie True si je garde la main pour la ligne suivante,
     * Renvoie False si je suis terminé (j'ai trouvé mon accolade fermante)
     * @param $key
     * @param $value
     * @return bool
     */
    public function manage($key, $value)
    {
        $result = true;
        if ($this->currentObject === self::OBJ_ME) {
            switch ($key) {
                case 'bn':
                    $this->setBn($value);
                    break;
                case 'fem':
                    $this->setFem($value);
                    break;
                case 'b_d': // Date de naissance
                    $this->setBd($value);
                    break;
                case 'att':
                    $this->setAtt($value);
                    break;
                case 'traits':
                    $this->setTraits($value);
                    break;
                case 'rel':
                    $this->setRel($value);
                    break;
                case 'cul':
                    $this->setCul($value);
                    break;
                case 'dna':
                    $this->setDna($value);
                    break;
                case 'prp':
                    $this->setPrp($value);
                    break;
                case 'fer': // Fertilité
                    $this->setFer($value);
                    break;
                case 'health':
                    $this->setHealth($value);
                    break;
                case 'prs':
                    $this->setPrs($value);
                    break;
                case 'piety':
                    $this->setPiety($value);
                    break;
                case 'job':
                    $this->setJob($value);
                    break;
                case 'wealth':
                    $this->setWealth($value);
                    break;
                case 'emp':
                    $this->setEmp($value);
                    break;
                case 'host':
                    $this->setHost($value);
                    break;
                case 'emi':
                    $this->setEmi($value);
                    break;
                case 'eme':
                    $this->setEme($value);
                    break;
                case 'eyi':
                    $this->setEyi($value);
                    break;
                case 'eypi':
                    $this->setEypi($value);
                    break;
                case 'lgr':
                    $this->setLgr(new CharacterLgr());
                    $this->currentObject = self::OBJ_LGR;
                    break;
                case 'action':
                    $this->setAction($value);
                    break;
                case 'acdate':
                    $this->setAcdate($value);
                    break;
                case 'cpos':
                    $this->setCpos($value);
                    break;
                case 'cpos_start':
                    $this->setCposStart($value);
                    break;
                case 'society':
                    $this->currentObject = self::OBJ_SOCIETY;
                    break;
                case 'fat':
                    $this->setFat($value);
                    break;
                case 'mot':
                    $this->setMot($value);
                    break;
                case 'dnt':
                    $this->setDnt($value);
                    break;
                case 'flags':
                    $this->setFlags(new Flags());
                    $this->currentObject = self::OBJ_FLAGS;
                    break;
                case 'md':
                    $this->addMd(new CharacterMd());
                    $this->currentObject = self::OBJ_MD;
                    break;
                case 'rfat':
                    $this->setRfat($value);
                    break;
                case 'bstd':
                    $this->setBstd($value);
                    break;
                case 'claim':
                    $this->addClaim(new Claim());
                    $this->currentObject = self::OBJ_CLAIM;
                    break;
                case 'title':
                    $this->setTitle($value);
                    break;
                case 'gov':
                    $this->setGov($value);
                    break;
                case 'regent':
                    $this->setRegent($value);
                    break;
                case 'guardian':
                    $this->setGuardian($value);
                    break;
                case 'known_plots':
                    $this->currentObject = self::OBJ_KNOWN_PLOTS;
                    break;
                case 'ambition_date':
                    $this->setAmbitionDate($value);
                    break;
                case 'focus_date':
                    $this->setFocusDate($value);
                    break;
                case 'score':
                    $this->setScore($value);
                    break;
                case 'dmn':
                    $this->setDmn(new Domain());
                    $this->currentObject = self::OBJ_DOMAIN;
                    break;
                case 'acloc':
                    $this->setAcloc($value);
                    break;
                case 'spouse':
                    $this->setSpouse($value);
                    break;
                case 'btrh':
                    $this->setBtrh($value);
                    break;
                case 'g_cul':
                    $this->setGCul($value);
                    break;
                case 'retrat':
                    $this->setRetrat($value);
                    break;
                case 'nick':
                    $this->setNick($value);
                    break;
                case 'lover':
                    $this->setLover($value);
                    break;
                case 'unborn':
                    $this->currentObject = self::OBJ_UNBORN;
                    break;
                case 'war_target':
                    $this->setWarTarget($value);
                    break;
                case 'last_objective':
                    $this->setLastObjective($value);
                    break;
                case 'council_discontent_start':
                    $this->setCouncilDiscontentStart($value);
                    break;
                case 'council_discontent_update':
                    $this->setCouncilDiscontentUpdate($value);
                    break;
                case 'council_discontent_reason':
                    $this->setCouncilDiscontentReason($value);
                    break;
                case 'imprisoned':
                    $this->setImprisoned($value);
                    break;
                case 'weak':
                    $this->setWeak($value);
                    break;
                case 'spawned_bastards':
                    $this->setSpawnedBastards($value);
                    break;
                case 'vars':
                    $this->setVars(new Vars());
                    $this->currentObject = self::OBJ_VARS;
                    break;
                case 'curinc':
                    $this->setCurinc($value);
                    break;
                case 'in_hiding':
                    $this->setInHiding($value);
                    break;




                case '{':
                    // Si on a un unborn, alors c'est la { d'ouverture d'un autre unborn
                    if( $this->unborns && count($this->unborns) > 0) {
                        $this->currentObject = self::OBJ_UNBORN;
                    }
                    break;
                case '}':
                    $result = false;
                    break;
                default:
                    echo 'CHARACTER unknown property !!! => ';
                    echo $key . ' => ' . $value . '<br />';
            }
            return $result;
        } else {
            $continue = true ;
            switch ($this->currentObject) {
                case self::OBJ_LGR:
                    $continue = $this->getLgr()->manage($key, $value);
                    break;
                case self::OBJ_FLAGS:
                    $continue = $this->getFlags()->manage($key, $value);
                    break;
                case self::OBJ_MD:
                    $continue = $this->getLastMd()->manage($key, $value);
                    break;
                case self::OBJ_CLAIM:
                    $continue = $this->getLastClaim()->manage($key, $value);
                    break;
                case self::OBJ_DOMAIN:
                    $continue = $this->getDmn()->manage($key, $value);
                    break;
                case self::OBJ_UNBORN:
                    if ($key === '{' && $this->level < 2) {
                        $this->level++;
                        if ($this->level === 2) {
                            $this->addUnborn(new Unborn());
                        }
                    } else {
                        // Si level = 2, on est dans un unborn et on appelle son manager
                        if ($this->level === 2) {
                            $continue = $this->getLastUnborn()->manage($key, $value);
                            // Si continue vaut false, c'est qu'on a fermé le $unborn, on descend au level 1
                            if (!$continue) {
                                $this->level = 1 ;
                                $continue = true ;
                            }
                        } else {
                            // level vaut 0 ou 1. Si 0, on a un pb (?). Si 1, on attend une fermeture
                            if ($key === '}') {
                                $this->level = 0 ;
                                $continue = false ;
                            }
                        }
                    }


                    break;
                case self::OBJ_VARS:
                    $continue = $this->getVars()->manage($key, $value);
                    break;

                default:
                    $returnToMe = false ;
                    if ($key === '{') {
                        return true ;
                    }
                    if ($key === '}') {
                        $this->currentObject = self::OBJ_ME;
                        return true ;
                    }
                    $values = explode(' ', $key);

                    if (trim($values[count($values)-1]) === '}') {
                        $returnToMe = true ;
                        unset($values[count($values)-1]);
                    }
                    switch ($this->currentObject) {
                        case self::OBJ_SOCIETY ;
                            $this->setSociety($values) ;
                            break;
                        case self::OBJ_KNOWN_PLOTS:
                            $this->setKnownPlots($values);
                            break;
                        default:
                            echo 'CHARACTER Sub-object ('.$this->currentObject.') unknown property !!! => ';
                            echo $key . ' => ' . $value . '<br />';
                    }
                    if ($returnToMe) {
                        $this->currentObject = self::OBJ_ME;
                    }
            }
            // Si je ne continue pas avec le player, je reviens sur moi
            if (!$continue) {
                $this->currentObject = self::OBJ_ME ;
            }
        }

        return $result ;
    }

}
