<?php
/**
 * Created by PhpStorm.
 * User: xyeh657
 * Date: 11/01/2019
 * Time: 14:13
 */

class CharacterLgr
{
    const OBJ_ME        = 'ME';
    const OBJ_INCOME    = 'INCOME';
    const OBJ_THIS_MONTH_INCOME = 'THIS_MONTH_INCOME' ;
    const OBJ_EXPENSE   = 'EXPENSE';
    const OBJ_LAST_MONTH_INCOME_TABLE = 'LAST_MONTH_INCOME_TABLE' ;
    const OBJ_LAST_MONTH_EXPENSE_TABLE = 'LAST_MONTH_INCOME_TABLE' ;

    protected $income ;
    protected $thisMonthIncome ;
    protected $lastMonthIncomeTable ;
    protected $lastMonthIncome ;
    protected $expense ;
    protected $lastMonthExpenseTable ;
    protected $lastMonthExpense ;

    protected $currentObject = self::OBJ_ME ;

    /**
     * @return mixed
     */
    public function getIncome()
    {
        return $this->income;
    }

    /**
     * @param mixed $income
     * @return CharacterLgr
     */
    public function setIncome($income)
    {
        $this->income = $income;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getThisMonthIncome()
    {
        return $this->thisMonthIncome;
    }

    /**
     * @param mixed $thisMonthIncome
     * @return CharacterLgr
     */
    public function setThisMonthIncome($thisMonthIncome)
    {
        $this->thisMonthIncome = $thisMonthIncome;
        return $this;
    }



    /**
     * @return mixed
     */
    public function getLastMonthIncomeTable()
    {
        return $this->lastMonthIncomeTable;
    }

    /**
     * @param mixed $lastMonthIncomeTable
     * @return CharacterLgr
     */
    public function setLastMonthIncomeTable($lastMonthIncomeTable)
    {
        $this->lastMonthIncomeTable = $lastMonthIncomeTable;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLastMonthIncome()
    {
        return $this->lastMonthIncome;
    }

    /**
     * @param mixed $lastMonthIncome
     * @return CharacterLgr
     */
    public function setLastMonthIncome($lastMonthIncome)
    {
        $this->lastMonthIncome = $lastMonthIncome;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getExpense()
    {
        return $this->expense;
    }

    /**
     * @param mixed $expense
     * @return CharacterLgr
     */
    public function setExpense($expense)
    {
        $this->expense = $expense;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLastMonthExpenseTable()
    {
        return $this->lastMonthExpenseTable;
    }

    /**
     * @param mixed $lastMonthExpenseTable
     * @return CharacterLgr
     */
    public function setLastMonthExpenseTable($lastMonthExpenseTable)
    {
        $this->lastMonthExpenseTable = $lastMonthExpenseTable;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLastMonthExpense()
    {
        return $this->lastMonthExpense;
    }

    /**
     * @param mixed $lastMonthExpense
     * @return CharacterLgr
     */
    public function setLastMonthExpense($lastMonthExpense)
    {
        $this->lastMonthExpense = $lastMonthExpense;
        return $this;
    }




    /**
     * Renvoie True si je garde la main pour la ligne suivante,
     * Renvoie False si je suis terminé (j'ai trouvé mon accolade fermante)
     * @param $key
     * @param $value
     * @return bool
     */
    public function manage($key, $value)
    {
        $continue = true;
        if ($this->currentObject === self::OBJ_ME) {
            switch ($key) {
                case 'income':
                    $this->currentObject = self::OBJ_INCOME;
                    break;
                case 'thismonthincome':
                    $this->currentObject = self::OBJ_THIS_MONTH_INCOME;
                    break;
                case 'lastmonthincometable':
                    $this->currentObject = self::OBJ_LAST_MONTH_INCOME_TABLE;
                    break;
                case 'lastmonthincome':
                    $this->setLastMonthIncome($value);
                    break;
                case 'expense':
                    $this->currentObject = self::OBJ_EXPENSE;
                    break;
                case 'lastmonthexpensetable':
                    $this->currentObject = self::OBJ_LAST_MONTH_EXPENSE_TABLE;
                    break;
                case 'lastmonthexpense':
                    $this->setLastMonthExpense($value);
                    break;
                case '{':
                    break;
                case '}':
                    $continue = false;
                    break;
                default:
                    echo 'CHARACTER LGR unknown property !!! => ';
                    echo $key . ' => ' . $value . '<br />';
            }
        } else {
            $returnToMe = false ;
            if ($key === '{') {
                return true ;
            }
            if ($key === '}') {
                $this->currentObject = self::OBJ_ME;
                return true ;
            }
            $values = explode(' ', $key);

            if (trim($values[count($values)-1]) === '}') {
                $returnToMe = true ;
                unset($values[count($values)-1]);
            }
            switch ($this->currentObject) {
                case self::OBJ_INCOME :
                    $this->setIncome($values) ;
                    break;
                case self::OBJ_THIS_MONTH_INCOME :
                    $this->setThisMonthIncome($values) ;
                    break;
                case self::OBJ_EXPENSE:
                    $this->setExpense($values) ;
                    break;
                case self::OBJ_LAST_MONTH_INCOME_TABLE:
                    $this->setLastMonthIncomeTable($values);
                    break;
                case self::OBJ_LAST_MONTH_EXPENSE_TABLE:
                    $this->setLastMonthExpenseTable($values);
                    break;
                default:
                    echo 'CHARACTER LGR Subobject ('.$this->currentObject.') unknown property !!! => ';
                    echo $key . ' => ' . $value . '<br />';
            }
            if ($this->currentObject === self::OBJ_INCOME) {
                $this->setIncome($values) ;
            } else {
                $this->setLastMonthIncomeTable($values);
            }
            if ($returnToMe) {
                $this->currentObject = self::OBJ_ME;
            }
        }

        return $continue;
    }



}
