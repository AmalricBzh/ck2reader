<?php
/**
 * Created by PhpStorm.
 * User: xyeh657
 * Date: 11/01/2019
 * Time: 15:05
 */

class CharacterMd
{
    protected $modifier ;
    protected $date;
    protected $hidden ;
    protected $inherit ;

    /**
     * @return mixed
     */
    public function getModifier()
    {
        return $this->modifier;
    }

    /**
     * @param mixed $modifier
     * @return CharacterMd
     */
    public function setModifier($modifier)
    {
        $this->modifier = $modifier;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     * @return CharacterMd
     */
    public function setDate($date)
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHidden()
    {
        return $this->hidden;
    }

    /**
     * @param mixed $hidden
     * @return CharacterMd
     */
    public function setHidden($hidden)
    {
        $this->hidden = $hidden;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getInherit()
    {
        return $this->inherit;
    }

    /**
     * @param mixed $inherit
     * @return CharacterMd
     */
    public function setInherit($inherit)
    {
        $this->inherit = $inherit;
        return $this;
    }






    /**
     * Renvoie True si je garde la main pour la ligne suivante,
     * Renvoie False si je suis terminé (j'ai trouvé mon accolade fermante)
     * @param $key
     * @param $value
     * @return bool
     */
    public function manage($key, $value) {
        $result = true ;
        switch ($key) {
            case 'modifier':
                $this->setModifier($value);
                break;
            case 'date':
                $this->setDate($value);
                break ;
            case 'hidden':
                $this->setHidden($value);
                break ;
            case 'inherit':
                $this->setInherit($value);
                break ;

            case '{':
                break;
            case '}':
                $result = false ;
                break;
            default:
                echo 'CHARACTER MD unknown property !!! => ' ;
                echo $key . ' => ' . $value .'<br />';
        }
        return $result;
    }
}
