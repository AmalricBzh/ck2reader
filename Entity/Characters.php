<?php
/**
 * Created by PhpStorm.
 * User: xyeh657
 * Date: 11/01/2019
 * Time: 13:39
 */

include_once 'Character.php';

class Characters
{
    const OBJ_ME        = 'ME' ;
    const OBJ_CHARACTER = 'CHARACTER' ;

    protected $currentObject = self::OBJ_ME;
    protected $lastCharacterId ;

    // Les characters, c'est un tableau associatif de character par id
    /** @var Character[] */
    protected $characters = [];

    public function addCharacter($id) {
        $this->characters[$id] = new Character() ;
        $this->lastCharacterId = $id;
        return $this;
    }

    public function getCharacter($id)  {
        return $this->characters[$id];
    }

    /**
     * Renvoie True si je garde la main pour la ligne suivante,
     * Renvoie False si je suis terminé (j'ai trouvé mon accolade fermante)
     * @param $key
     * @param $value
     * @return bool
     */
    public function manage($key, $value) {
        $result = true ;
        if ($this->currentObject === self::OBJ_ME) {
            switch ($key) {
                case '{':
                    break;
                case '}':
                    $result = false;
                    break;
                default:
                    $this->addCharacter($key);
                    $this->currentObject = self::OBJ_CHARACTER;
            }
        } else {
            $continue = $this->characters[$this->lastCharacterId]->manage($key, $value);
            // Si je ne continue pas avec l'objet, je reviens sur moi
            if (!$continue) {
                $this->currentObject = self::OBJ_ME ;
            }
        }
        return $result;
    }
}
