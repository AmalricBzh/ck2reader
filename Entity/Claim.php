<?php
/**
 * Created by PhpStorm.
 * User: xyeh657
 * Date: 11/01/2019
 * Time: 15:20
 */

class Claim
{
    protected $title ;
    protected $pressed;
    protected $weak;
    protected $is_dynamic ;

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     * @return Claim
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPressed()
    {
        return $this->pressed;
    }

    /**
     * @param mixed $pressed
     * @return Claim
     */
    public function setPressed($pressed)
    {
        $this->pressed = $pressed;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getWeak()
    {
        return $this->weak;
    }

    /**
     * @param mixed $weak
     * @return Claim
     */
    public function setWeak($weak)
    {
        $this->weak = $weak;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getisDynamic()
    {
        return $this->is_dynamic;
    }

    /**
     * @param mixed $is_dynamic
     * @return Claim
     */
    public function setIsDynamic($is_dynamic)
    {
        $this->is_dynamic = $is_dynamic;
        return $this;
    }





    /**
     * Renvoie True si je garde la main pour la ligne suivante,
     * Renvoie False si je suis terminé (j'ai trouvé mon accolade fermante)
     * @param $key
     * @param $value
     * @return bool
     */
    public function manage($key, $value) {
        $result = true ;
        switch ($key) {
            case 'title':
                $this->setTitle($value);
                break;
            case 'pressed':
                $this->setPressed($value);
                break;
            case 'weak':
                $this->setWeak($value);
                break;
            case 'is_dynamic':
                $this->setIsDynamic($value);
                break ;
            case '{':
                break;
            case '}':
                $result = false ;
                break;
            default:
                echo 'CLAIM unknown property !!! => ' ;
                echo $key . ' => ' . $value .'<br />';
        }
        return $result;
    }
}
