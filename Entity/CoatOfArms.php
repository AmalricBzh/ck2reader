<?php
/**
 * Created by PhpStorm.
 * User: xyeh657
 * Date: 11/01/2019
 * Time: 12:48
 */

include_once 'CoatOfArmsData.php';

class CoatOfArms
{

    const OBJ_ME =      'ME' ;
    const OBJ_DATA =    'DATA';

    protected $data ;
    protected $religion;

    protected $currentObject = self::OBJ_ME;

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     * @return CoatOfArms
     */
    public function setData($data)
    {
        $this->data = $data;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getReligion()
    {
        return $this->religion;
    }

    /**
     * @param mixed $religion
     * @return CoatOfArms
     */
    public function setReligion($religion)
    {
        $this->religion = $religion;
        return $this;
    }

    /**
     * Renvoie True si je garde la main pour la ligne suivante,
     * Renvoie False si je suis terminé (j'ai trouvé mon accolade fermante)
     * @param $key
     * @param $value
     * @return bool
     */
    public function manage($key, $value)
    {
        $result = true;
        if ($this->currentObject === self::OBJ_ME) {
            switch ($key) {
                case 'data':
                    $this->setData(new CoatOfArmsData());
                    $this->currentObject = self::OBJ_DATA;
                    break;
                case 'religion':
                    $this->setReligion($value);
                    break;
                case '{':
                    break;
                case '}':
                    $result = false;
                    break;
                default:
                    echo 'COAT OF ARMS unknown property !!! => ';
                    echo $key . ' => ' . $value . '<br />';
            }
        } else {
            $continue = $this->getData()->manage($key, $value);
            // Si je ne continue pas avec le player, je reviens sur moi
            if (!$continue) {
                $this->currentObject = self::OBJ_ME ;
            }
        }

        return $result;
    }


}
