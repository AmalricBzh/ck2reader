<?php
/**
 * Created by PhpStorm.
 * User: xyeh657
 * Date: 11/01/2019
 * Time: 13:00
 */

class CoatOfArmsData
{

    // array of values separated by spaces
    protected $data = [] ;

    public function manage($key, $value) {

        if ($key === '}') {
            return false;
        }
        if ($key === '{') {
            return true;
        }
        $continue = true ;
        $values = explode(' ', $key);

        //var_dump($values);die;
        if (trim($values[count($values)-1]) === '}') {
            $continue = false ;
            unset($values[count($values)-1]);
        }
        $this->data = $values ;
        return $continue;
    }
}
