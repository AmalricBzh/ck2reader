<?php
/**
 * Created by PhpStorm.
 * User: xyeh657
 * Date: 11/01/2019
 * Time: 16:03
 */

include_once 'Troops.php' ;
include_once 'Army.php' ;

class Domain
{
    const OBJ_ME =      'ME' ;
    const OBJ_LIEGE_TROOPS = 'LIEGE_TROOPS' ;
    const OBJ_RAISED_LIEGE_TROOPS = 'RAISED_LIEGE_TROOPS' ;
    const OBJ_LIEGELEVY_REINFORCEMENTS = 'LIEGELEVY_REINFORCEMENTS';
    const OBJ_ARMY = 'ARMY' ;

    protected $currentObject  = self::OBJ_ME;

    protected $capital ;
    protected $myLiegelevyContribution ;
    protected $liegelevyReinforcements ;
    protected $liegeTroops ;
    protected $army ;
    protected $raised_liege_troops ;
    protected $militaryTechpoints ;
    protected $economyTechpoints ;
    protected $cultureTechpoints ;
    protected $peace_months ;
    protected $primary ;

    /**
     * @return mixed
     */
    public function getCapital()
    {
        return $this->capital;
    }

    /**
     * @param mixed $capital
     * @return Domain
     */
    public function setCapital($capital)
    {
        $this->capital = $capital;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMyLiegelevyContribution()
    {
        return $this->myLiegelevyContribution;
    }

    /**
     * @param mixed $myLiegelevyContribution
     * @return Domain
     */
    public function setMyLiegelevyContribution($myLiegelevyContribution)
    {
        $this->myLiegelevyContribution = $myLiegelevyContribution;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLiegeTroops()
    {
        return $this->liegeTroops;
    }

    /**
     * @param mixed $liegeTroops
     * @return Domain
     */
    public function setLiegeTroops($liegeTroops)
    {
        $this->liegeTroops = $liegeTroops;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getArmy()
    {
        return $this->army;
    }

    /**
     * @param mixed $army
     * @return Domain
     */
    public function setArmy($army)
    {
        $this->army = $army;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRaisedLiegeTroops()
    {
        return $this->raised_liege_troops;
    }

    /**
     * @param mixed $raised_liege_troops
     * @return Domain
     */
    public function setRaisedLiegeTroops($raised_liege_troops)
    {
        $this->raised_liege_troops = $raised_liege_troops;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMilitaryTechpoints()
    {
        return $this->militaryTechpoints;
    }

    /**
     * @param mixed $militaryTechpoints
     * @return Domain
     */
    public function setMilitaryTechpoints($militaryTechpoints)
    {
        $this->militaryTechpoints = $militaryTechpoints;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEconomyTechpoints()
    {
        return $this->economyTechpoints;
    }

    /**
     * @param mixed $economyTechpoints
     * @return Domain
     */
    public function setEconomyTechpoints($economyTechpoints)
    {
        $this->economyTechpoints = $economyTechpoints;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCultureTechpoints()
    {
        return $this->cultureTechpoints;
    }

    /**
     * @param mixed $cultureTechpoints
     * @return Domain
     */
    public function setCultureTechpoints($cultureTechpoints)
    {
        $this->cultureTechpoints = $cultureTechpoints;
        return $this;
    }

    /**
     * @return string
     */
    public function getCurrentObject()
    {
        return $this->currentObject;
    }

    /**
     * @param string $currentObject
     * @return Domain
     */
    public function setCurrentObject($currentObject)
    {
        $this->currentObject = $currentObject;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPeaceMonths()
    {
        return $this->peace_months;
    }

    /**
     * @param mixed $peace_months
     * @return Domain
     */
    public function setPeaceMonths($peace_months)
    {
        $this->peace_months = $peace_months;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPrimary()
    {
        return $this->primary;
    }

    /**
     * @param mixed $primary
     * @return Domain
     */
    public function setPrimary($primary)
    {
        $this->primary = $primary;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLiegelevyReinforcements()
    {
        return $this->liegelevyReinforcements;
    }

    /**
     * @param mixed $liegelevyReinforcements
     * @return Domain
     */
    public function setLiegelevyReinforcements($liegelevyReinforcements)
    {
        $this->liegelevyReinforcements = $liegelevyReinforcements;
        return $this;
    }









    /**
     * Renvoie True si je garde la main pour la ligne suivante,
     * Renvoie False si je suis terminé (j'ai trouvé mon accolade fermante)
     * @param $key
     * @param $value
     * @return bool
     */
    public function manage($key, $value)
    {
        $result = true;
        if ($this->currentObject === self::OBJ_ME) {
            switch ($key) {
                case 'capital':
                    $this->setCapital($value);
                    break;
                case 'my_liegelevy_contribution':
                    $this->setMyLiegelevyContribution($value);
                    break;
                case 'liege_troops':
                    $this->setLiegeTroops(new Troops());
                    $this->currentObject = self::OBJ_LIEGE_TROOPS;
                    break;
                case 'army':
                    $this->setArmy(new Army());
                    $this->currentObject = self::OBJ_ARMY;
                    break;
                case 'raised_liege_troops':
                    $this->currentObject = self::OBJ_RAISED_LIEGE_TROOPS;
                    break ;
                case 'military_techpoints':
                    $this->setMilitaryTechpoints($value);
                    break;
                case 'economy_techpoints':
                    $this->setEconomyTechpoints($value);
                    break;
                case 'culture_techpoints':
                    $this->setCultureTechpoints($value);
                    break;
                case 'peace_months':
                    $this->setPeaceMonths($value);
                    break;
                case 'primary':
                    $this->setPrimary($value);
                    break;
                case 'liegelevy_reinforcements':
                    $this->currentObject = self::OBJ_LIEGELEVY_REINFORCEMENTS;
                    break ;

                case '{':
                    break;
                case '}':
                    $result = false;
                    break;
                default:
                    echo 'DOMAIN unknown property => ';
                    echo $key . ' => ' . $value . '<br />';
            }
            return $result;
        } else {
            $continue = true ;
            switch ($this->currentObject) {

                case self::OBJ_LIEGE_TROOPS:
                    $continue = $this->getLiegeTroops()->manage($key, $value);
                    break;
                case self::OBJ_ARMY:
                    $continue = $this->getArmy()->manage($key, $value);
                    break;

                default:
                    if ($key === '{') {
                        return true ;
                    }
                    if ($key === '}') {
                        $this->currentObject = self::OBJ_ME;
                        return true ;
                    }

                    $returnToMe = false ;

                    $values = explode(' ', $key);

                    if (trim($values[count($values)-1]) === '}') {
                        $returnToMe = true ;
                        unset($values[count($values)-1]);
                    }
                    switch ($this->currentObject) {
                        case self::OBJ_RAISED_LIEGE_TROOPS ;
                            $this->setRaisedLiegeTroops($values) ;
                            break;
                        case self::OBJ_LIEGELEVY_REINFORCEMENTS ;
                            $this->setLiegelevyReinforcements($values) ;
                            break;
                        default:
                            echo 'DOMAIN Sub-object ('.$this->currentObject.') unknown property !!! => ';
                            echo $key . ' => ' . $value . '<br />';
                    }
                    if ($returnToMe) {
                        $this->currentObject = self::OBJ_ME;
                    }
            }
            // Si je ne continue pas avec le player, je reviens sur moi
            if (!$continue) {
                $this->currentObject = self::OBJ_ME ;
            }
        }

        return $result ;
    }
}
