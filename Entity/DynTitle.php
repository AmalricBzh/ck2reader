<?php
/**
 * Created by PhpStorm.
 * User: xyeh657
 * Date: 11/01/2019
 * Time: 11:47
 */

class DynTitle
{

    protected $title ;
    protected $baseTitle ;
    protected $isCustom ;
    protected $isDynamic ;

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     * @return DynTitle
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBaseTitle()
    {
        return $this->baseTitle;
    }

    /**
     * @param mixed $baseTitle
     * @return DynTitle
     */
    public function setBaseTitle($baseTitle)
    {
        $this->baseTitle = $baseTitle;
        return $this;
    }



    /**
     * @return mixed
     */
    public function getisCustom()
    {
        return $this->isCustom;
    }

    /**
     * @param mixed $isCustom
     * @return DynTitle
     */
    public function setIsCustom($isCustom)
    {
        $this->isCustom = $isCustom;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getisDynamic()
    {
        return $this->isDynamic;
    }

    /**
     * @param mixed $isDynamic
     * @return DynTitle
     */
    public function setIsDynamic($isDynamic)
    {
        $this->isDynamic = $isDynamic;
        return $this;
    }



    /**
     * Renvoie True si je garde la main pour la ligne suivante,
     * Renvoie False si je suis terminé (j'ai trouvé mon accolade fermante)
     * @param $key
     * @param $value
     * @return bool
     */
    public function manage($key, $value) {
        $result = true ;
        switch ($key) {
            case 'title':
                $this->setTitle($value);
                break;
            case 'base_title':
                $this->setBaseTitle($value);
                break ;
            case 'is_custom':
                $this->setIsCustom($value);
                break;
            case 'is_dynamic':
                $this->setIsDynamic($value);
                break;
            case '{':
                break;
            case '}':
                $result = false ;
                break;
            default:
                echo 'DYN TITLE unknown property !!! => ' ;
                echo $key . ' => ' . $value .'<br />';
        }
        return $result;
    }

}
