<?php
/**
 * Created by PhpStorm.
 * User: xyeh657
 * Date: 11/01/2019
 * Time: 12:28
 */

include_once 'Dynasty.php';

class Dynasties
{
    const OBJ_ME        = 'Dynasties:me' ;
    const OBJ_DYNASTY   = 'Dinasties:Dynasty' ;

    protected $currentObject = Dynasties::OBJ_ME;
    protected $lastDynastyId ;

    // Les dynasties, c'est un tableau associatif de dynasties par id
    /** @var Dynasty[] */
    protected $dynasties = [];

    public function addDynasty($id) {
        $this->dynasties[$id] = new Dynasty() ;
        return $this;
    }

    public function getDynasty($id)  {
        return $this->dynasties[$id];
    }

    /**
     * Renvoie True si je garde la main pour la ligne suivante,
     * Renvoie False si je suis terminé (j'ai trouvé mon accolade fermante)
     * @param $key
     * @param $value
     * @return bool
     */
    public function manage($key, $value) {
        $result = true ;
        if ($this->currentObject === Dynasties::OBJ_ME) {
            switch ($key) {
                case '{':
                    break;
                case '}':
                    $result = false;
                    break;
                default:
                    $this->addDynasty($key);
                    $this->lastDynastyId = $key;
                    $this->currentObject = Dynasties::OBJ_DYNASTY;
            }
        } else {
            $continue = $this->dynasties[$this->lastDynastyId]->manage($key, $value);
            // Si je ne continue pas avec le player, je reviens sur moi
            if (!$continue) {
                $this->currentObject = Dynasties::OBJ_ME ;
            }
        }
        return $result;
    }
}

