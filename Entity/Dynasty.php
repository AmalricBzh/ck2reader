<?php
/**
 * Created by PhpStorm.
 * User: xyeh657
 * Date: 11/01/2019
 * Time: 12:18
 */

include_once 'CoatOfArms.php';

class Dynasty
{
    protected $id;
    protected $name;
    protected $culture;
    protected $religion ;
    protected $coat_of_arms ;
    protected $decadence ;
    protected $setCoatOfArms;
    protected $isModified ;

    const OBJ_ME            = 'ME' ;
    const OBJ_COAT_OF_ARMS  = 'COAT_OF_ARMS' ;

    protected $currentObject = Dynasty::OBJ_ME;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Dynasty
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Dynasty
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCulture()
    {
        return $this->culture;
    }

    /**
     * @param mixed $culture
     * @return Dynasty
     */
    public function setCulture($culture)
    {
        $this->culture = $culture;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getReligion()
    {
        return $this->religion;
    }

    /**
     * @param mixed $religion
     * @return Dynasty
     */
    public function setReligion($religion)
    {
        $this->religion = $religion;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCoatOfArms()
    {
        return $this->coat_of_arms;
    }

    /**
     * @param mixed $coat_of_arms
     * @return Dynasty
     */
    public function setCoatOfArms($coat_of_arms)
    {
        $this->coat_of_arms = $coat_of_arms;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDecadence()
    {
        return $this->decadence;
    }

    /**
     * @param mixed $decadence
     * @return Dynasty
     */
    public function setDecadence($decadence)
    {
        $this->decadence = $decadence;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSetCoatOfArms()
    {
        return $this->setCoatOfArms;
    }

    /**
     * @param mixed $setCoatOfArms
     * @return Dynasty
     */
    public function setSetCoatOfArms($setCoatOfArms)
    {
        $this->setCoatOfArms = $setCoatOfArms;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getisModified()
    {
        return $this->isModified;
    }

    /**
     * @param mixed $isModified
     * @return Dynasty
     */
    public function setIsModified($isModified)
    {
        $this->isModified = $isModified;
        return $this;
    }







    /**
     * Renvoie True si je garde la main pour la ligne suivante,
     * Renvoie False si je suis terminé (j'ai trouvé mon accolade fermante)
     * @param $key
     * @param $value
     * @return bool
     */
    public function manage($key, $value)
    {
        $result = true;
        if ($this->currentObject === self::OBJ_ME) {
            switch ($key) {
                case 'name':
                    $this->setName($value);
                    break;
                case 'culture':
                    $this->setCulture($value);
                    break;
                case 'religion':
                    $this->setReligion($value);
                    break;
                case 'coat_of_arms':
                    $this->setCoatOfArms(new CoatOfArms());
                    $this->currentObject = self::OBJ_COAT_OF_ARMS;
                    break;
                case 'decadence':
                    $this->setDecadence($value);
                    break;
                case 'set_coat_of_arms':
                    $this->setSetCoatOfArms($value);
                    break;
                case 'is_modified':
                    $this->setIsModified($value);
                    break;
                case '{':
                    break;
                case '}':
                    $result = false;
                    break;
                default:
                    echo 'DYNASTY unknown property !!! => ';
                    echo $key . ' => ' . $value . '<br />';
            }
            return $result;
        } else {
            $continue = $this->getCoatOfArms()->manage($key, $value);
            // Si je ne continue pas avec le player, je reviens sur moi
            if (!$continue) {
                $this->currentObject = self::OBJ_ME ;
            }
        }

        return $result ;
    }


}
