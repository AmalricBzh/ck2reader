<?php
/**
 * Created by PhpStorm.
 * User: xyeh657
 * Date: 11/01/2019
 * Time: 12:02
 */

class Flags
{

    // Les flags, c'est un flag, une date. Donc on se fait un tableau associatif
    protected $flags = [];

    public function addFlag($flag, $date) {
        $this->flags[$flag] = $date ;
        return $this;
    }

    public function getFlags()  {
        return $this->flags;
    }

    /**
     * Renvoie True si je garde la main pour la ligne suivante,
     * Renvoie False si je suis terminé (j'ai trouvé mon accolade fermante)
     * @param $key
     * @param $value
     * @return bool
     */
    public function manage($key, $value) {
        $result = true ;
        switch ($key) {
            case '{':
                break;
            case '}':
                $result = false ;
                break;
            default:
                $this->addFlag($key, $value);
        }
        return $result;
    }
}
