<?php
/**
 * Created by PhpStorm.
 * User: xyeh657
 * Date: 11/01/2019
 * Time: 11:32
 */

class GameRules
{



    /**
     * Renvoie True si je garde la main pour la ligne suivante,
     * Renvoie False si je suis terminé (j'ai trouvé mon accolade fermante)
     * @param $key
     * @param $value
     * @return bool
     */
    public function manage($key, $value) {
        $result = true ;
        switch ($key) {
            case '{':
                break;
            case '}':
                $result = false ;
                break;
            default:
                echo 'GAME RULES unknown property !!! => ' ;
                echo $key . ' => ' . $value .'<br />';
        }
        return $result;
    }
}
