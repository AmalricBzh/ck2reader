<?php
/**
 * Created by PhpStorm.
 * User: xyeh657
 * Date: 11/01/2019
 * Time: 16:15
 */

class Troops
{

    const OBJ_ME = 'ME' ;
    const light_infantry_f = 'LIF';
    const heavy_infantry_f = 'HIF';
    const pikemen_f = 'PF';
    const light_cavalry_f = 'LCF';
    const knights_f = 'KF';
    const archers_f = 'AF';
    const horse_archers = 'HF';
    const galleys_f = 'GF';
    const camel_cavalry = 'CC';
    const war_elephants = 'WE' ;

    protected $currentObject = self::OBJ_ME ;

    protected $light_infantry_f ;
    protected $heavy_infantry_f ;
    protected $pikemen_f ;
    protected $light_cavalry_f ;
    protected $knights_f ;
    protected $archers_f ;
    protected $horse_archers ;
    protected $galleys_f ;
    protected $camel_cavalry ;
    protected $war_elephants ;

    /**
     * @return mixed
     */
    public function getLightInfantryF()
    {
        return $this->light_infantry_f;
    }

    /**
     * @param mixed $light_infantry_f
     * @return Troops
     */
    public function setLightInfantryF($light_infantry_f)
    {
        $this->light_infantry_f = $light_infantry_f;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHeavyInfantryF()
    {
        return $this->heavy_infantry_f;
    }

    /**
     * @param mixed $heavy_infantry_f
     * @return Troops
     */
    public function setHeavyInfantryF($heavy_infantry_f)
    {
        $this->heavy_infantry_f = $heavy_infantry_f;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPikemenF()
    {
        return $this->pikemen_f;
    }

    /**
     * @param mixed $pikemen_f
     * @return Troops
     */
    public function setPikemenF($pikemen_f)
    {
        $this->pikemen_f = $pikemen_f;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLightCavalryF()
    {
        return $this->light_cavalry_f;
    }

    /**
     * @param mixed $light_cavalry_f
     * @return Troops
     */
    public function setLightCavalryF($light_cavalry_f)
    {
        $this->light_cavalry_f = $light_cavalry_f;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getKnightsF()
    {
        return $this->knights_f;
    }

    /**
     * @param mixed $knights_f
     * @return Troops
     */
    public function setKnightsF($knights_f)
    {
        $this->knights_f = $knights_f;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getArchersF()
    {
        return $this->archers_f;
    }

    /**
     * @param mixed $archers_f
     * @return Troops
     */
    public function setArchersF($archers_f)
    {
        $this->archers_f = $archers_f;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHorseArchers()
    {
        return $this->horse_archers;
    }

    /**
     * @param mixed $horse_archers
     * @return Troops
     */
    public function setHorseArchers($horse_archers)
    {
        $this->horse_archers = $horse_archers;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGalleysF()
    {
        return $this->galleys_f;
    }

    /**
     * @param mixed $galleys_f
     * @return Troops
     */
    public function setGalleysF($galleys_f)
    {
        $this->galleys_f = $galleys_f;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCamelCavalry()
    {
        return $this->camel_cavalry;
    }

    /**
     * @param mixed $camel_cavalry
     * @return Troops
     */
    public function setCamelCavalry($camel_cavalry)
    {
        $this->camel_cavalry = $camel_cavalry;
        return $this;
    }

    /**
     * @return string
     */
    public function getCurrentObject()
    {
        return $this->currentObject;
    }

    /**
     * @param string $currentObject
     * @return Troops
     */
    public function setCurrentObject($currentObject)
    {
        $this->currentObject = $currentObject;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getWarElephants()
    {
        return $this->war_elephants;
    }

    /**
     * @param mixed $war_elephants
     * @return Troops
     */
    public function setWarElephants($war_elephants)
    {
        $this->war_elephants = $war_elephants;
        return $this;
    }






    /**
     * Renvoie True si je garde la main pour la ligne suivante,
     * Renvoie False si je suis terminé (j'ai trouvé mon accolade fermante)
     * @param $key
     * @param $value
     * @return bool
     */
    public function manage($key, $value) {
        $result = true ;

        // Si la valeur n'est pas nulle, alors on a proprieté=valeur
        if ($value && $this->currentObject === self::OBJ_ME) {

            switch ($key) {
                case 'light_infantry_f':
                    $this->setLightInfantryF($value);
                    break;
                case 'heavy_infantry_f':
                    $this->setHeavyInfantryF($value);
                    break;
                case 'pikemen_f':
                    $this->setPikemenF($value);
                    break;
                case 'light_cavalry_f':
                    $this->setLightCavalryF($value);
                    break;
                case 'knights_f':
                    $this->setKnightsF($value);
                    break;
                case 'archers_f':
                    $this->setArchersF($value);
                    break;
                case 'horse_archers':
                    $this->setHorseArchers($value);
                    break;
                case 'galleys_f':
                    $this->setGalleysF($value);
                    break;
                case 'camel_cavalry':
                    $this->setCamelCavalry($value);
                    break;
                case 'war_elephants':
                    $this->setWarElephants($value);
                    break;

                case '{':
                    break;
                case '}':
                    $result = false;
                    break;
                default:
                    echo 'TROOPS single unknown property !!! => ';
                    echo $key . ' => ' . $value . '<br />';
            }
        } elseif ($this->currentObject === self::OBJ_ME) {
            //On est dans le cas proprieté =
            //  {
            // valeur1 valeur2 }
            switch ($key) {
                case 'light_infantry_f':
                    $this->currentObject = self::light_infantry_f;
                    break;
                case 'heavy_infantry_f':
                    $this->currentObject = self::heavy_infantry_f;
                    break;
                case 'pikemen_f':
                    $this->currentObject = self::pikemen_f;
                    break;
                case 'light_cavalry_f':
                    $this->currentObject = self::light_cavalry_f;
                    break;
                case 'knights_f':
                    $this->currentObject = self::knights_f;
                    break;
                case 'archers_f':
                    $this->currentObject = self::archers_f;
                    break;
                case 'horse_archers':
                    $this->currentObject = self::horse_archers;
                    break;
                case 'galleys_f':
                    $this->currentObject = self::galleys_f;
                    break;
                case 'camel_cavalry':
                    $this->currentObject = self::camel_cavalry ;
                    break;
                case 'war_elephants':
                    $this->currentObject = self::war_elephants ;
                    break;
                case '{':
                    break;
                case '}':
                    $result = false;
                    break;
                default:
                    echo 'TROOPS double property unknown property !!! => ';
                    echo $key . ' => ' . $value . '<br />';

            }
        } else {
            // on est censé avoir la valeur...

            $returnToMe = false ;
            $values = explode(' ', $key);

            if (trim($values[count($values)-1]) === '}') {
                $returnToMe = true ;
                unset($values[count($values)-1]);
            }

            switch($this->currentObject) {
                case self::light_infantry_f :
                    $this->setLightInfantryF($values);
                    break ;
                case self::heavy_infantry_f :
                    $this->setHeavyInfantryF($values);
                    break ;
                case self::pikemen_f :
                    $this->setPikemenF($values);
                    break;
                case self::light_cavalry_f :
                    $this->setLightCavalryF($values);
                    break;
                case self::knights_f :
                    $this->setKnightsF($values);
                    break;
                case self::archers_f :
                    $this->setArchersF($values);
                    break;
                case self::horse_archers :
                    $this->setHorseArchers($values);
                    break;
                case self::galleys_f :
                    $this->setGalleysF($values);
                    break;
                case self::camel_cavalry :
                    $this->setCamelCavalry($values);
                    break;
                case self::war_elephants :
                    $this->setWarElephants($values);
                    break;
                default :
                    echo 'TROOPS double Sub-object ('.$this->currentObject.') unknown property !!! => ';
                    echo $key . ' => ' . $value . '<br />';
            }
            if ($returnToMe) {
                $this->currentObject = self::OBJ_ME;

            }
        }


        return $result;
    }

}
