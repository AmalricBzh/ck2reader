<?php
/**
 * Created by PhpStorm.
 * User: Amaury
 * Date: 11/01/2019
 * Time: 19:56
 */

include_once 'UnbornParent.php';

class Unborn
{
    const OBJ_ME =      'ME' ;
    const OBJ_MOTHER =  'MOTHER' ;
    const OBJ_FATHER =  'FATHER' ;

    private $currentObject = self::OBJ_ME ;

    protected $mother ;
    protected $father ;
    protected $date ;

    /**
     * @return mixed
     */
    public function getMother()
    {
        return $this->mother;
    }

    /**
     * @param mixed $mother
     * @return Unborn
     */
    public function setMother($mother)
    {
        $this->mother = $mother;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFather()
    {
        return $this->father;
    }

    /**
     * @param mixed $father
     * @return Unborn
     */
    public function setFather($father)
    {
        $this->father = $father;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     * @return Unborn
     */
    public function setDate($date)
    {
        $this->date = $date;
        return $this;
    }






    /**
     * Renvoie True si je garde la main pour la ligne suivante,
     * Renvoie False si je suis terminé (j'ai trouvé mon accolade fermante)
     * @param $key
     * @param $value
     * @return bool
     */
    public function manage($key, $value) {
        $result = true;
        if ($this->currentObject === self::OBJ_ME) {
            switch ($key) {
                case 'mother':
                    $this->setMother(new UnbornParent());
                    $this->currentObject = self::OBJ_MOTHER;
                    break;
                case 'father':
                    $this->setFather(new UnbornParent());
                    $this->currentObject = self::OBJ_FATHER;
                    break;
                case 'date':
                    $this->setDate($value);
                    break;

                case '{':
                    break;
                case '}':
                    $result = false;
                    break;
                default:
                    echo 'UNBORN unknown property !!! => ';
                    echo $key . ' => ' . $value . '<br />';
            }
        } else {
            $continue = true ;
            switch ($this->currentObject) {
                case self::OBJ_MOTHER:
                    $continue = $this->getMother()->manage($key, $value);
                    break;
                case self::OBJ_FATHER:
                    $continue = $this->getFather()->manage($key, $value);
                    break;

                default:
                    echo 'CHARACTER Sub-object ('.$this->currentObject.') unknown property !!! => ';
                    echo $key . ' => ' . $value . '<br />';
            }
            // Si je ne continue pas avec le player, je reviens sur moi
            if (!$continue) {
                $this->currentObject = self::OBJ_ME ;
            }
        }
        return $result;
    }
}
