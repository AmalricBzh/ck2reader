<?php
/**
 * Created by PhpStorm.
 * User: Amaury
 * Date: 11/01/2019
 * Time: 21:52
 */

class Vars
{
    // Les vars, c'est une propriété, une valeur. On se fait donc un tableau associatif
    protected $vars = [];

    public function addVar($var, $value) {
        $this->vars[$var] = $value ;
        return $this;
    }

    public function getVars()  {
        return $this->vars;
    }

    /**
     * Renvoie True si je garde la main pour la ligne suivante,
     * Renvoie False si je suis terminé (j'ai trouvé mon accolade fermante)
     * @param $key
     * @param $value
     * @return bool
     */
    public function manage($key, $value) {
        $result = true ;
        switch ($key) {
            case '{':
                break;
            case '}':
                $result = false ;
                break;
            default:
                $this->addVar($key, $value);
        }
        return $result;
    }
}