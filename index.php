<?php


$startTime = microtime(true);

include_once 'Entity/CK2Backup.php';

// Open backup file
$fh =fopen ('data/1360_01_24.ck2', 'r');
if (!$fh) {
    echo 'Error: cannot open file';
    die;
}

// Read the first line. Must be "CK2txt"
if (trim(fgets($fh)) !== 'CK2txt') {
    echo 'Error: file is not a CK2 backup';
    die;
}

// read other lines
$currentLine = 0 ;
$errors = 0 ;

$ck2 = new CK2Backup();


$line = fgets($fh);
while ($line !== false) {
    // Saute les lignes vides
    if (trim($line) === '') {
        $line = fgets($fh);
        continue;
    }
    $currentLine ++ ;

    // Clef suivi de = ?
    $keyValue = explode('=', $line);
    $key = trim($keyValue[0]);
    $value = null ;
    if (count($keyValue) > 1) {
        $value = trim($keyValue[1]) ;
    }

    $result = $ck2->manage($key, $value);
    // Si true, la ligne a été traitée
    if (!$result) {
        $errors ++ ;
    }

    $line = fgets($fh);
    // Pour l'instant on ne fait que les XX premières lignes ou on s'arrête après YY lignes inconnues
    if ($currentLine > 200000 || $errors > 15) {
        $line = false ;
    }
}

fclose($fh);

$endTime = microtime(true) - $startTime ;

$endTime=round($endTime, 2);

$percent = round($currentLine * 100 / 4632651,2) ;

echo "Duration : $endTime <br />" ;
echo "Line read : $currentLine ($percent %)<br />" ;

echo '<br /><br /><hr /><br /><pre>';
var_dump($ck2);

